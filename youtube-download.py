from pytube import YouTube
import re

url = input("Provide URL: ")

video = YouTube(url)

print('Found video: '+ video.title)
streams = video.streams.filter(progressive=True)

streamlist = []

for i in streams:
    streamlist.append(i)

print(streamlist)